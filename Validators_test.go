package Validate

import (
	"fmt"
	"testing"
)


/*
   Test the VerifyFormer that are available.

   todo: The new validation BO's fields process.
   We will be reusing existing Validators - we will be

	"Required":     "Can not be empty",
	   "Required_AA":  "The field %s is required",
	"Min":          "Minimum is %d",
	"Max":          "Maximum is %d",
	"Range":        "Range is %d to %d",
	"MinSize":      "Minimum size is %d",
	"MaxSize":      "Maximum size is %d",
	"Length":       "Required length is %d",
	"Alpha":        "Must be valid alpha characters",
	"Numeric":      "Must be valid numeric characters",
	"AlphaNumeric": "Must be valid alpha or numeric characters",
	"Match":        "Must match %s",
	"NoMatch":      "Must not match %s",
	"AlphaDash":    "Must be valid alpha or numeric or dash(-_) characters",
	"Email":        "Must be a valid email address",
	"IP":           "Must be a valid ip address",
	"Base64":       "Must be valid base64 characters",
	"Mobile":       "Must be valid mobile number",
	"Tel":          "Must be valid telephone number",
	"Phone":        "Must be valid telephone or mobile phone number",
	"ZipCode":      "Must be valid zipcode",
       Postcode
*/



// normal required
func Test_VRequired(t *testing.T) {
	fmt.Println("Test_VRequired")

	// Test validation behaviour
	err := Verify("", "Name", []Vfn{VRequired})
	if err == nil {
		t.Error("Expecting Failure but it passed")
	}

	// Hmm should strip blanks out " " is not a valid required field in most cases.
	//	err = vfValidate(" ", "Name", []Validfunc{vfRequired})
	//	if err == nil {
	//		t.Error("Expecting Failure but it passed")
	//	}

	err = Verify("Fred", "Name", []Vfn{VRequired})
	if err != nil {
		t.Errorf("Expecting Non Failure but it gave em this %v ",err)
	}

}
// required - with messaging customized.


func Test_VEmail(t *testing.T) {
	fmt.Println("Test_VEmail")

	// Email
	err := Verify("not@a email", "email", []Vfn{VRequired,VEmail})
	if err == nil {
		t.Error("Expecting Failure but it passed Email ")
	}

	err = Verify("test@gmail.com", "email", []Vfn{VRequired,VEmail})
	if err != nil {
		t.Errorf("Not Expecting an Error msg %v ", err)
	}

}

/*
func Test_VName(t *testing.T) {
	fmt.Println("Test_VMaxsize")

	// todo: hook up a simple func for Maxsize
	// Ok the problem is Vrequired is a simple func
	// VMaxsize needs to pass a Maxsize struct
// think about it and see if I can adjust it easily.
	// the problem is that the Vfn is just a flat func so it doesn't work
	// Maxsize(0,10) - closure?

	// VRequired(v *VerifyFormer) error
	v := func(v *VerifyFormer) error { return v.Maxsize(v,10) }

	err := Verify("John", "Name", []Vfn{ v  })
	if err != nil {
		return err
	}

	err = Verify("Johnabcdedfghijklmnop", "Name", []Vfn{validation.MaxSize{10}})
	if err != nil {
		return err
	}

	// Max field size cannot be exceeded
	//	"MaxSize":      "Maximum size is %d",
	//	"Length":       "Required length is %d",
	//	"Alpha":        "Must be valid alpha characters",

}
*/
// Invalid characters


// Test further


