package Validate

// Build here new Field Verification funcs

// todo : setup an example of what the this file can do.



import (
	"fmt"
	"reflect"
	"regexp"
	"time"
)



//type VerifyData struct {
//	Fieldname string
//}

type VerifyFormer struct {
	FieldName  string
	FieldValue interface{}
}

// Copied off validators but Extended
var VTemplates = map[string]string{
	"Required":     "%s can not be empty",
	"Min":          "Minimum is %d",
	"Max":          "Maximum is %d",
	"Range":        "Range is %d to %d",
	"MinSize":      "Minimum size is %d",
	"MaxSize":      "Maximum size is %d",
	"Length":       "Required length is %d",
	"Alpha":        "Must be valid alpha characters",
	"Numeric":      "Must be valid numeric characters",
	"AlphaNumeric": "Must be valid alpha or numeric characters",
	"Match":        "Must match %s",
	"NoMatch":      "Must not match %s",
	"AlphaDash":    "Must be valid alpha or numeric or dash(-_) characters",
	"Email":        "Must be a valid email address",
	"IP":           "Must be a valid ip address",
	"Base64":       "Must be valid base64 characters",
	"Mobile":       "Must be valid mobile number",
	"Tel":          "Must be valid telephone number",
	"Phone":        "Must be valid telephone or mobile phone number",
	"ZipCode":      "Must be valid zipcode",
}

type Vfn func(*VerifyFormer) error

func Verify(value interface{}, field string, vfns []Vfn) error {
	loVerifyFormer := new(VerifyFormer)
	loVerifyFormer.FieldName = field
	loVerifyFormer.FieldValue = value
	if len(vfns) > 0 {
		for _, Item := range vfns {
			if err := Item(loVerifyFormer); err != nil {
				return err
			}
		}
	}
	return nil
}

// I want to be able to extend Default message easily and if ne
// also put in place custom funcs to verify against. - though they all should be written so they can be reused.
func VRequired(v *VerifyFormer) error {
	if !v.IsSatisfied(v.FieldValue) {
		return fmt.Errorf(VTemplates["Required"], v.FieldName)
	} else {
		return nil
	}
}



func VMaxSize(v *VerifyFormer, MaxLength int) error {
	// if length is too great then Error
	// "Max":          "Maximum is %d",
	loMS := new(MaxSize)
	loMS.Max = MaxLength

//	MaxSize
   if !loMS.IsSatisfied(v.FieldValue) {
		return fmt.Errorf(VTemplates["MaxSize"], v.FieldName, MaxLength )
	} else {
		return nil
	}
}


//var emailPattern = regexp.MustCompile("[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[a-zA-Z0-9](?:[\\w-]*[\\w])?")

//type Email struct {
//	Match
//	Key string
//}

// Make sure it is a valid Email
func VEmail(v *VerifyFormer) error {
	Matched, err := regexp.MatchString("[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[a-zA-Z0-9](?:[\\w-]*[\\w])?", fmt.Sprintf("%v", v.FieldValue))
	//_, err := emailPattern.MatchString(fmt.Sprintf("%v", v.FieldValue))
	//m.Regexp.MatchString(fmt.Sprintf("%v", obj))
	if !Matched {
		return fmt.Errorf(VTemplates["Email"], v.FieldName)
	}

	if err != nil {
		return fmt.Errorf(VTemplates["Email"], v.FieldName)
	}
	return nil
}

func (r *VerifyFormer) IsSatisfied(obj interface{}) bool {
	if obj == nil {
		return false
	}

	if str, ok := obj.(string); ok {
		return len(str) > 0
	}
	if b, ok := obj.(bool); ok {
		return b
	}
	if i, ok := obj.(int); ok {
		return i != 0
	}
	if t, ok := obj.(time.Time); ok {
		return !t.IsZero()
	}
	v := reflect.ValueOf(obj)
	if v.Kind() == reflect.Slice {
		return v.Len() > 0
	}
	return true
}

//func (v *Validation) Email(obj interface{}, key string) *ValidationResult {
//	return v.apply(Email{Match{Regexp: emailPattern}, key}, obj)
//}
